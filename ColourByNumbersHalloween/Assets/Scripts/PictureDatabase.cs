﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PictureDatabase : MonoBehaviour
{
    [System.Serializable]
     public class PictureSet
    {
        public string nameOfPicture;
        public List<Color> imageColourList = new List<Color>();
        public List<Color> colourPalleteList = new List<Color>();

        public List<int> imageNumberList = new List<int>();

        public PictureSet(string name, List<Color> imCol, List<Color> colPal, List<int> imNum){
            nameOfPicture = name;
            imageColourList = imCol;
            colourPalleteList = colPal;
            imageNumberList = imNum;
        }
    }

    public PictureSet myPicSet;

    //public List<PictureSet> SetsOfPictureData = new List<PictureSet>();


    public void AddToDatabase (string currentImage, List<Color> pictureColours, List<Color> currentColourPallete,List<int> imNumList){
        myPicSet.nameOfPicture = currentImage;
        myPicSet.imageColourList = pictureColours;
        myPicSet.colourPalleteList = currentColourPallete;
        myPicSet.imageNumberList = imNumList;
    }
}
