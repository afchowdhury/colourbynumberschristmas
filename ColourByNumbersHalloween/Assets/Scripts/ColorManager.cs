﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.InteropServices;

public class ColorManager : MonoBehaviour
{
    public Color currentColour;
    public Color defaultColour;

    public List<Color> allTheColours = new List<Color>();
    public Image[] buttonImages;
    public List<Color> colourList = new List<Color>();
    public List<int> numList = new List<int>();

    public TileInfo[] squaresInfo;
    public int currentNum;

    public bool mDown;

    public List <PictureDatabase> differentImages  = new List<PictureDatabase>();
    public int randData;

    public int correctAmount;
    private string nameOfImage;
    public GameObject nextButton;
    public UIManager uiMan;
    private DataCarrier data;
    public bool done;


    void Start()
    {
        done = false;
        nextButton.SetActive(false);
        data = GameObject.FindGameObjectWithTag("DataCarrier").GetComponent<DataCarrier>();
        randData = data.chosenPicture;
        //ChooseNew();
        SetUpGame();
        CheckCorrect();
        foreach (var item in squaresInfo)
        {
            item.GetComponent<Image>().color = defaultColour;
                item.currentNum = 40;
        }
        
    }

    private void Update() {

        if(Input.GetButtonDown("Fire1")){
            mDown = true;
        }

        if(Input.GetButtonUp("Fire1")){
            mDown = false;
        }


    }

    void SetUpGame(){


        allTheColours = differentImages[randData].myPicSet.colourPalleteList;
        colourList = differentImages[randData].myPicSet.imageColourList;
        numList = differentImages[randData].myPicSet.imageNumberList;
        nameOfImage = differentImages[randData].myPicSet.nameOfPicture;


        currentColour = allTheColours[0];
        for (int i = 0; i < buttonImages.Length; i++)
        {
            buttonImages[i].color = allTheColours[i];
        }

        for (int i = 0; i < squaresInfo.Length; i++)
        {
            squaresInfo[i].myColour = colourList[i];
            squaresInfo[i].expectedNum = numList[i];
            squaresInfo[i].correct = false;
            squaresInfo[i].SetUp();

        }

        differentImages.RemoveAt(randData);
    }

    // Update is called once per frame
    public void ChangeNum (int newNum){
        currentNum = newNum;
        currentColour = allTheColours[currentNum];
    }

    public void ChooseNew (){

        randData = Random.Range(0,differentImages.Count);

    }

    public void CheckCorrect (){
        correctAmount = 0;
        foreach (var item in squaresInfo)
        {
            
             if(item.correct){
                 correctAmount ++;
            }
            else{
                StartCoroutine(ChangeColour(item));


                // item.GetComponent<Image>().color = Color.red;
                // item.currentNum = 40;
                
            }
        }

        if(correctAmount == squaresInfo.Length){
            Debug.Log("Win");
            nextButton.SetActive(true);
                foreach (var item in squaresInfo){
                    item.textNum.text = " ";
                }
                done = true;
        }
        else{
            nextButton.SetActive(false);
        }
    }

    private IEnumerator ChangeColour(TileInfo item)
    {
        if(item.correct){
                yield return new WaitForSeconds(1);
                correctAmount ++;
            }
            else{
                item.GetComponent<Image>().color = Color.red;
                item.currentNum = 40;
                yield return new WaitForSeconds(1);
                item.GetComponent<Image>().color = defaultColour;
            }
            
    }
    

    public void NextImage (){
        uiMan.EndScreen();
        // if(pictureDatabase.SetsOfPictureData.Count > 0){
        //     ChooseNew();
        //     SetUpGame();
        //     CheckCorrect();
        // }
        // else{
        //     Debug.Log("Win");
        // }
    }

    public void DestroyData (){
        Destroy(data.gameObject);
    }

}
