﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DataCarrier : MonoBehaviour
{
    public int chosenPicture;
    
    public void SetPicture (int chosen){
        chosenPicture = chosen;
        SceneManager.LoadScene("Game");

    }
    void Start()
    {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("DataCarrier");

        if (objs.Length > 1)
        {
            Destroy(this.gameObject);
        }

        DontDestroyOnLoad(this.gameObject);
    }
}
