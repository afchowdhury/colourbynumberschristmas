﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PictureStore : MonoBehaviour
{
    // Start is called before the first frame update
    // [System.Serializable]
    // public class PictureSet
    // {
    //     public string nameOfPicture;
    //     public List<Color> imageColourList = new List<Color>();
    //     public List<Color> colourPalleteList = new List<Color>();

    //     public List<int> imageNumberList = new List<int>();

    //     public PictureSet(string name, List<Color> imCol, List<Color> colPal, List<int> imNum){
    //         nameOfPicture = name;
    //         imageColourList = imCol;
    //         colourPalleteList = colPal;
    //         imageNumberList = imNum;
    //     }
    // }

    public PictureDatabase pictureDatabase;

    //public List<PictureSet> SetsOfPictureData = new List<PictureSet>();
    public List<Color> currentColourPallete = new List<Color>();
    public List<Color> pictureColours = new List<Color>();
    public List<int> imNumList = new List<int>();
    public List<EditorTiles> tiles = new List<EditorTiles>();

    public int currentNum;
    public Color currentColour;
    public Image[] buttonImages;
    public string currentImage;

     public bool mDown;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.F)){
            UpdateColourButtons();
            foreach (var item in tiles)
        {
            item.currentNum = 1;
            item.ChangeColor();
        }
        }

        if(Input.GetKeyDown(KeyCode.S)){
            SaveInfo();
        }

          if(Input.GetButtonDown("Fire1")){
            mDown = true;
        }

        if(Input.GetButtonUp("Fire1")){
            mDown = false;
        }


    }

    public void ChangeNum (int newNum){
        currentNum = newNum;
        currentColour = currentColourPallete[currentNum];
    }

    public void UpdateColourButtons(){
        for (int i = 0; i < buttonImages.Length; i++)
        {
            buttonImages[i].color = currentColourPallete[i];
        }
    }

    public void SaveInfo (){
        imNumList.Clear();
        pictureColours.Clear();
        foreach (var item in tiles)
        {
            
            imNumList.Add(item.currentNum);
            pictureColours.Add(currentColourPallete[item.currentNum-1]);
        }

        pictureDatabase.AddToDatabase(currentImage, pictureColours, currentColourPallete, imNumList);
    }
}
