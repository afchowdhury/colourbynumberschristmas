﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class EditorTiles : MonoBehaviour, IPointerEnterHandler
    
{
    public PictureStore picMan;
    public int currentNum;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeColor (){

        GetComponent<Image>().color = picMan.currentColour;
        currentNum = picMan.currentNum+1;


    }

     public void OnPointerEnter(PointerEventData eventData)
     {
         if(picMan.mDown){
             ChangeColor();
         }
     }
}
